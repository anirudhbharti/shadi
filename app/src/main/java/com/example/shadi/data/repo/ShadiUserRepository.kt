package com.example.shadi.data.repo

import com.example.shadi.data.local.UserDao
import com.example.shadi.data.remote.UserRemoteDataSource
import com.example.shadi.util.performGetOperation
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class ShadiUserRepository @Inject constructor(
    private val remoteDataSource: UserRemoteDataSource,
    private val localDataSource: UserDao
) {

    fun getUsersList() = performGetOperation(
        databaseQuery = { localDataSource.getUserList() },
        networkCall = { remoteDataSource.getUsers() },
        saveCallResult = {
            localDataSource.clearTable()
            localDataSource.insertAllUsers(it.results)
        }
    )

    fun saveAcceptStatus(id: String) {
        GlobalScope.launch {
            localDataSource.updateUserAcceptStatus(id, true)
        }
    }

    fun saveRejectStatus(id: String) {
        GlobalScope.launch {
            localDataSource.updateUserRejectStatus(id, true)
        }
    }

}

