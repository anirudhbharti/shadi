package com.example.shadi.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shadi.data.entities.UserEntity


@Dao
interface UserDao {
    @Query("UPDATE user SET accept = :accept  WHERE email = :id")
    fun updateUserAcceptStatus(id: String, accept: Boolean)

    @Query("UPDATE user SET reject = :reject WHERE email = :id")
    fun updateUserRejectStatus(id: String, reject: Boolean)

    @Query("SELECT * FROM user")
    fun getUserList(): LiveData<List<UserEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUsers(userProps: List<UserEntity>)

    @Query("DELETE FROM user")
    fun clearTable()
}