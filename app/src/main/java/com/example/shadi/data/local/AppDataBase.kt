package com.example.shadi.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.shadi.data.entities.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
@TypeConverters(JsonConverters::class, PictureConverters::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun userDao(): UserDao

}