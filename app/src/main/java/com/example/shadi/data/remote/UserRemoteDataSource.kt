package com.example.shadi.data.remote

import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val apiService: ApiService) :
    BaseDataSource() {
    suspend fun getUsers() = getResult { apiService.getData() }

}