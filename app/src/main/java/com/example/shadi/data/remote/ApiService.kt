package com.example.shadi.data.remote

import com.example.shadi.data.entities.UserDataResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("api/?results=20")
    suspend fun getData(): Response<UserDataResponse>

}