package com.example.shadi.data.local

import androidx.room.TypeConverter
import com.example.shadi.data.entities.Picture
import com.example.shadi.data.entities.UserName
import com.google.gson.Gson

internal class JsonConverters {

    @TypeConverter
    fun appToString(app: UserName): String = Gson().toJson(app)

    @TypeConverter
    fun stringToApp(string: String): UserName = Gson().fromJson(string, UserName::class.java)

}

internal class PictureConverters {

    @TypeConverter
    fun appToString(app: Picture): String = Gson().toJson(app)

    @TypeConverter
    fun stringToApp(string: String): Picture = Gson().fromJson(string, Picture::class.java)

}