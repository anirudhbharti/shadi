package com.example.shadi.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class UserDataResponse(val results: List<UserEntity>)

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey()
    @ColumnInfo(name = "email") var email: String,
    @ColumnInfo(name = "name") var name: UserName,
    @ColumnInfo(name = "accept") var accept: Boolean,
    @ColumnInfo(name = "reject") var reject: Boolean,
    @ColumnInfo(name = "picture") var picture: Picture
)

data class UserName(val title: String, val first: String, val last: String)

data class Picture(val large: String, val medium: String, val thumbnail: String)


