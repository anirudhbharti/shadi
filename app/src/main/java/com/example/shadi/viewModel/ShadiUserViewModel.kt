package com.example.shadi.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.shadi.data.entities.UserEntity
import com.example.shadi.data.repo.ShadiUserRepository
import com.example.shadi.util.Resource

class ShadiUserViewModel constructor(
    private val userRepo: ShadiUserRepository
) : ViewModel() {
    fun getUserListLiveData(): LiveData<Resource<List<UserEntity>>> {
        return userRepo.getUsersList()
    }

    fun saveAcceptStatus(id: String) {
        userRepo.saveAcceptStatus(id)
    }

    fun saveDeclineStatus(id: String) {
        userRepo.saveRejectStatus(id)
    }
}
