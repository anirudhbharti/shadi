package com.example.shadi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shadi.data.repo.ShadiUserRepository
import com.example.shadi.viewModel.ShadiUserViewModel
import javax.inject.Inject

class ShadiViewModelFactory @Inject constructor(
    private var userRepository: ShadiUserRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShadiUserViewModel(userRepository) as T
    }
}