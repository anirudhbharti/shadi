package com.example.shadi.di

import android.app.Application
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.shadi.data.local.AppDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomDatabaseModule(private var application: Application) {
    private val databaseCallback = object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            Log.d("RoomDatabaseModule", "onCreate")
        }
    }


    @Singleton
    @Provides
    fun providesRoomDatabase(): AppDataBase {
        return Room.databaseBuilder(
            application,
            AppDataBase::class.java,
            "User"
        ).fallbackToDestructiveMigration()
            .build()
    }

}