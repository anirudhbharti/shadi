package com.example.shadi.di

import com.example.shadi.data.local.AppDataBase
import com.example.shadi.data.local.UserDao
import com.example.shadi.data.remote.ApiService
import com.example.shadi.data.remote.UserRemoteDataSource
import com.example.shadi.data.repo.ShadiUserRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object AppModule {

    @Provides
    @Singleton
    fun providesRetrofitInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://randomuser.me/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideRemoteApi(apiService: ApiService) = UserRemoteDataSource(apiService)


    @Provides
    @Singleton
    fun provideEndPoints(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()


    @Singleton
    @Provides
    fun provideUserDao(db: AppDataBase) = db.userDao()

    @Singleton
    @Provides
    fun provideRepository(
        apiHelper: UserRemoteDataSource,
        userDao: UserDao
    ) = ShadiUserRepository(apiHelper, userDao)

}