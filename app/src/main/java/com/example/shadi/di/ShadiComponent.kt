package com.example.shadi.di

import com.example.shadi.ui.ShadiUserActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, RoomDatabaseModule::class])
interface ShadiComponent {
    fun inject(shadiUserActivity: ShadiUserActivity)
}