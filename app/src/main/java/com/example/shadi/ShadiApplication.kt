package com.example.shadi

import android.app.Application
import com.example.shadi.di.DaggerShadiComponent
import com.example.shadi.di.RoomDatabaseModule
import com.example.shadi.di.ShadiComponent


class ShadiApplication : Application(){
    companion object {
        lateinit var instance: ShadiApplication
    }
    lateinit var shadiComponent: ShadiComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        shadiComponent = DaggerShadiComponent
            .builder()
            .roomDatabaseModule(RoomDatabaseModule(this))
            .build()
    }
}