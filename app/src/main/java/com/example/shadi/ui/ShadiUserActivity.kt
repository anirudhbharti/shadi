package com.example.shadi.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shadi.R
import com.example.shadi.ShadiApplication
import com.example.shadi.di.ShadiViewModelFactory
import com.example.shadi.util.Resource
import com.example.shadi.viewModel.ShadiUserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class ShadiUserActivity : AppCompatActivity(), ShadiUserAdapter.OnItemClickListener {

    private lateinit var userListViewModel: ShadiUserViewModel
    private lateinit var shadiUserAdapter: ShadiUserAdapter

    @Inject
    lateinit var shadiViewModelFactory: ShadiViewModelFactory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        injectDagger()
        createViewModel()
        setAdapter()
        observeUsersData()
    }

    private fun injectDagger() {
        ShadiApplication.instance.shadiComponent.inject(this)
    }

    private fun createViewModel() {
        userListViewModel =
            ViewModelProviders.of(this, shadiViewModelFactory)[ShadiUserViewModel::class.java]
    }


    private fun setAdapter() {
        shadiUserAdapter = ShadiUserAdapter()
        rvList.adapter = shadiUserAdapter
        rvList.layoutManager = LinearLayoutManager(this)
        shadiUserAdapter.setOnItemClickListener(this)
    }

    private fun observeUsersData() {
        userListViewModel.getUserListLiveData().observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    hideProgressBar()
                    shadiUserAdapter.submitList(it?.data)
                }
                Resource.Status.ERROR -> {
                    hideProgressBar()
                  //  Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING -> {
                    showProgressBar()
                }
            }
        })
    }

    override fun onAcceptClick(id: String) {
        userListViewModel.saveAcceptStatus(id)
    }

    override fun onRejectClick(id: String) {
        userListViewModel.saveDeclineStatus(id)
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }
}