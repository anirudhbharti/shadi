package com.example.shadi.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shadi.R
import com.example.shadi.data.entities.UserEntity
import com.example.shadi.util.UserListDataComparator
import kotlinx.android.synthetic.main.shadi_user_item.view.*


class ShadiUserAdapter :
    ListAdapter<UserEntity, ShadiUserAdapter.UserViewHolder>(UserListDataComparator()) {

    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.shadi_user_item, parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = getItem(position)
        if (user != null) {
            holder.bindTo(user)
        }
    }

    inner class UserViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindTo(userEntity: UserEntity) {
            view.tvName.text =
                (userEntity.name.title + " " + userEntity.name.first + " " + userEntity.name.last)
            view.tvEmail.text = (userEntity.email)
            if (userEntity.picture.medium.isNotEmpty())
                Glide.with(view.ivUser.context).load(userEntity.picture.medium).into(view.ivUser)

            if (!userEntity.accept && !userEntity.reject) {
                view.tvAccept.visibility = View.VISIBLE
                view.tvReject.visibility = View.VISIBLE
                itemView.tvAccept.text = itemView.tvAccept.context.getString(R.string.accept)
                itemView.tvReject.text = itemView.tvReject.context.getString(R.string.reject)
            } else {
                if (userEntity.accept) {
                    itemView.tvAccept.text = itemView.tvAccept.context.getString(R.string.accepted)
                    view.tvAccept.visibility = View.VISIBLE
                    view.tvReject.visibility = View.GONE
                } else if (userEntity.reject) {
                    itemView.tvReject.text = itemView.tvReject.context.getString(R.string.rejected)
                    view.tvReject.visibility = View.VISIBLE
                    view.tvAccept.visibility = View.GONE
                }
            }
            setClickListener(view, itemView, absoluteAdapterPosition)
        }
    }

    private fun setClickListener(
        view: View,
        itemView: View,
        absoluteAdapterPosition: Int
    ) {
        view.tvAccept.setOnClickListener {
            itemView.tvAccept.text = itemView.tvAccept.context.getString(R.string.accepted)
            itemView.tvReject.visibility = View.GONE
            getItem(absoluteAdapterPosition).accept = true
            onItemClickListener?.onAcceptClick(getItem(absoluteAdapterPosition).email)
        }
        view.tvReject.setOnClickListener {
            itemView.tvAccept.visibility = View.GONE
            itemView.tvReject.text = itemView.tvReject.context.getString(R.string.rejected)
            getItem(absoluteAdapterPosition).reject = true
            onItemClickListener?.onRejectClick(getItem(absoluteAdapterPosition).email)
        }
    }


    interface OnItemClickListener {
        fun onAcceptClick(id: String)
        fun onRejectClick(id: String)
    }
}
