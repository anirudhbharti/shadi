package com.example.shadi.util

import androidx.recyclerview.widget.DiffUtil
import com.example.shadi.data.entities.UserEntity

class UserListDataComparator : DiffUtil.ItemCallback<UserEntity>() {
        override fun areItemsTheSame(
            oldItem: UserEntity,
            newItem: UserEntity
        ): Boolean {
            return (oldItem == newItem)
        }

        override fun areContentsTheSame(
            oldItem: UserEntity,
            newItem: UserEntity
        ): Boolean {
            return (oldItem.email == newItem.email)
        }
    }